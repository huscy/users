# major.minor.patch
VERSION = (1, 3, 0)

__version__ = '.'.join(str(x) for x in VERSION)
